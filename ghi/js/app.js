


function createCard(name,description,pictureUrl){
    return `
<div class="card  shadow-lg">
<img src="${pictureUrl}" class="card-img-top" alt="...">
<div class="card-body">
    <h5 class="card-title">${name}</h5>
    <p class="card-text">${description}</p>
</div>
</div>
    `;
}



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try{
        const response = await fetch(url)

        if (!response.ok){
            throw new Error('error');
        }else{
            const data = await response.json()

            for (let conference of data.conferences){
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)
                if (detailResponse.ok){
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(title, description, pictureUrl);
                    const collum = document.querySelector(".col")
                    collum.innerHTML += html





                }
            }
            }
        }catch(error){
            console.log('done fucked up')
        }
});
