window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/states/';
  const stateResponse = await fetch(url);

  if (stateResponse.ok) {
    const data = await stateResponse.json();

    const selectTag = document.getElementById('state');
    const stateDrop = document.createElement('option');

    for (let state of data.states) {
      stateDrop.value = state.abbreviation;
      stateDrop.innerHTML = state.state;
      selectTag.appendChild(stateDrop);
    }
  }

  const formTag = document.getElementById('create-location-form');
  formTag.addEventListener('submit', async (event) => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));


    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      formTag.reset();
      const newLocation = await response.json();
    }
  });
});
